# Use an official PHP runtime as a base image
FROM php:8.1-apache

ENV DATABASE_URL=''

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf \
  \
     && apt-get update && \
        apt-get install --yes --force-yes \
        cron g++ gettext libicu-dev openssl \
        libc-client-dev libkrb5-dev  \
        libxml2-dev libfreetype6-dev \
        libgd-dev libmcrypt-dev bzip2 \
        libbz2-dev libtidy-dev libcurl4-openssl-dev \
        libz-dev libmemcached-dev libxslt-dev git-core libpq-dev \
        libzip4 libzip-dev libwebp-dev

# PHP Configuration
RUN docker-php-ext-install bcmath bz2 dba exif gettext iconv intl  soap tidy xsl zip &&\
    docker-php-ext-install mysqli pgsql pdo pdo_mysql pdo_pgsql calendar &&\
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp &&\
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl &&\
    docker-php-ext-install imap &&\
    docker-php-ext-configure hash --with-mhash &&\
    pecl install xdebug && docker-php-ext-enable xdebug


RUN curl -sS https://getcomposer.org/installer | php -- --disable-tls && mv composer.phar /usr/local/bin/composer

# Apache Configuration
COPY ./.docker/vhosts/vhosts.conf /etc/apache2/sites-enabled/000-default.conf 
RUN a2enmod rewrite 

WORKDIR /var/www/

COPY ../ .

RUN mkdir -p /var/www/var/log && mkdir -p /var/www/var/cache
RUN chown -R www-data:www-data /var/www/var/log
RUN chown -R www-data:www-data /var/www/var/cache

EXPOSE 80
