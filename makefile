#---SYMFONY--#
CONSOLE = php bin/console
DOCKER_COMPOSE = docker-compose
DOCKER_EXEC = docker exec
API_CONTAINER_NAME = todo_app_api
#------------#

#---YARN--#
YARN = yarn
#------------#

##
## HELP
help: ## Show this help.
	@echo "Command helper"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

# Application
info: ## Shows Php and application version
	@php --version
	@$(SYMFONY_CONSOLE) about

# install package in host
install-packages:
	@composer install --no-scripts

# build
build:
		$(DOCKER_COMPOSE) up -d

# init app
docker-app-init:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer init-db
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer init-db-test
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer config-app
#		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer exec-tasks

# cache clear 
clear-cache:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) $(CONSOLE) c:c

# run messenger 
run-messenger:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) composer exec-tasks

# run tests 
run-tests:
		$(DOCKER_EXEC) $(API_CONTAINER_NAME) php bin/phpunit --testdox

# build app (firt time)
build-app:
		@make install-packages
		@make build
		@make docker-app-init


.PHONY: info clear-cache docker-app-init install-packages build build-app run-messenger run-tests
